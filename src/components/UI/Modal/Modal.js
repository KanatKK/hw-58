import React from 'react';
import Backdrop from "../Backdrop/Backdrop";
import './Modal.css'

const Modal = props => (
    <>
        <Backdrop show={props.show} clicked={props.close}/>
        <div
            className="Modal"
            style={{
                transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                opacity : props.show ? '1' : '0'
            }}
        >
            <button type="button" className="closeModal" onClick={props.close}>X</button>
            <h3 className="modalHeading">{props.title}</h3>
            {props.children}
        </div>
    </>
);

export default Modal;