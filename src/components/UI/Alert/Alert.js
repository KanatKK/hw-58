import React from 'react';
import './Alert.css'
const Alert = props => (
    <div className={['alert', props.type].join(' ')} style={{display: props.style? 'block' : 'none'}}>
        <button className="delete" onClick={props.delete} style={{display: props.button? 'block' : 'none'}}>X</button>
        {props.children}
    </div>
);

export default Alert;