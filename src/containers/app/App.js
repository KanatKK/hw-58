import React, {useState} from 'react';
import './App.css';
import Modal from "../../components/UI/Modal/Modal";
import Alert from "../../components/UI/Alert/Alert";

const App = () => {

    const [showModal, setShowModal] = useState(false);

    const modalShowHandler = () => {
        setShowModal(true);
    };

    const modalCloseHandler = () => {
        setShowModal(false);
    };

    const [alerts, setAlerts] = useState([
        {type: 'primary', display: true,}, {type: 'success', display: true,},
        {type: 'warning', display: true}, {type: 'danger', display: true},
    ]);


    const deleteAlert = index => {
        let alertsCopy = [...alerts];
        alertsCopy[index].display = false;
        setAlerts(alertsCopy);
    };

    const [btn] = useState(true)

    return (
      <>
          <Modal
              title="Some kinda modal title"
              show={showModal} close={modalCloseHandler}
          >
          <p>This is modal content</p>
          </Modal>
          <button type="button" className="show" onClick={modalShowHandler}>Show modal</button>
          <Alert type={alerts[0].type} delete={() => deleteAlert(0)} style={alerts[0].display} button={btn}>
              <p>This is a {alerts[0].type} type alert</p>
          </Alert>
          <Alert type={alerts[1].type} delete={() => deleteAlert(1)} style={alerts[1].display}>
              <p>This is a {alerts[1].type} type alert</p>
          </Alert>
          <Alert type={alerts[2].type} delete={() => deleteAlert(2)} style={alerts[2].display} button={btn}>
              <p>This is a {alerts[2].type} type alert</p>
          </Alert>
          <Alert type={alerts[3].type} delete={() => deleteAlert(3)} style={alerts[3].display}>
              <p>This is a {alerts[3].type} type alert</p>
          </Alert>
      </>
  );
};

export default App;
